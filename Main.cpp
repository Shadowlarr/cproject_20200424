#include <iostream>

using namespace std;

#define SIZE 7

int main()
{

	cout << "Array" << '[' << SIZE << ']' << '[' << SIZE << ']' << '\n';

	int array[SIZE][SIZE] = { 0 };

	for (int i = 0; i < SIZE; ++i)
	{
		for (int j = 0; j < SIZE; ++j)
		{
			array[i][j] = i + j;
			cout << array[i][j] << ' ';
		}
		cout << '\n';
	}

	int Number = 24;
	cout << "Enter today's number: " << '\n';
	cin >> Number;
	Number = (Number % SIZE); 

	int Sum = 0;

	for (int i = SIZE - 1; i >= 0; --i)
	{
		Sum = Sum + array[Number][i];
	}

	cout << "Sum of row items: " << Sum << '\n';

	return 0;
}